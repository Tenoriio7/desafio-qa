import java.util.Random;

public class CarrinhoRegras {
	Integer totalCarrinho = 0;
	Integer itensCarrinho = 0;
	Integer bonusA = 0;
	Integer bonusB = 0;
	Integer valordoSorteio = 0;
	Integer valorA = 0;
	Integer valorB = 0;
	Integer valorC = 0;
	Integer valorD = 0;
	Integer quantidadeA = 0;
	Integer quantidadeB = 0;
	Integer quantidadeC = 0;
	Integer quantidadeD = 0;

	public void addItem(String item) {
		if (item.equals("A")) {
			quantidadeA++;
			calculaA();
		} else if (item.equals("B")) {
			++quantidadeB;
			calculaB();
		} else if (item.equals("C")) {
			++quantidadeC;
			calculaC();
		} else if (item.equals("D")) {
			++quantidadeD;
			calculaD();
		}
	}

	private void zeraValores() {
		totalCarrinho = itensCarrinho = quantidadeA = quantidadeB = quantidadeC = quantidadeD = valorA = valorB = valorC = valorD = bonusA = bonusB = 0;
	}

	public void calculaB() {
		if (bonusB == 0) {
			valorB = valorB + 30;
			System.out.println(
					"B inserido no carrinho com sucesso ! Definindo o valor unitario para  30, totalB: " + valorB);
			bonusB = 1;
		} else if (bonusB == 1) {
			valorB = valorB + 15;
			System.out.println(
					"B inserido no carrinho com sucesso ! Definindo o valor unitario para  15, totalB: " + valorB);
			bonusB = 0;
		}
	}

	public void calculaC() {
		valorC = valorC + 20;
		System.out
				.println("C inserido no carrinho com sucesso ! Definindo o valor unitario para  20, totalC: " + valorC);
	}

	public void calculaD() {
		valorD = valorD + 15;
		System.out
				.println("D inserido no carrinho com sucesso ! Definindo o valor unitario para  15, totalD: " + valorD);
	}

	public void fechaCarrinho() {
		totalCarrinho = valorA + valorB + valorC + valorD;
		itensCarrinho = quantidadeA + quantidadeB + quantidadeC + quantidadeD;
		System.out.println("\n -------------------------");
		System.out.println("Esse carrinho contem a quantidade " + itensCarrinho + " itens.");
		System.out.println("Valor total: " + totalCarrinho + " <<<");

		zeraValores();
	}

	public int sortQuantidadeDeItens() {
		Random sort = new Random();
		valordoSorteio = sort.nextInt((5) + 1);
		return valordoSorteio;
	}

	public void calculaA() {
		if (bonusA == 0) {
			valorA = valorA + 50;
			System.out.println(
					"A inserido no carrinho com sucesso ! Definindo o valor unitario para  50, totalA: " + valorA);
			bonusA = 1;
		} else if (bonusA == 1) {
			valorA = valorA + 50;
			System.out.println(
					"A inserido no carrinho com sucesso ! Definindo o valor unitario para  50, totalA: " + valorA);
			bonusA = 2;
		} else if (bonusA == 2) {
			valorA = valorA + 30;
			System.out.println(
					"A inserido no carrinho com sucesso ! Definindo o valor unitario para  30, totalA: " + valorA);
			bonusA = 0;
		}
	}

}
