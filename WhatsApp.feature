# encoding: utf-8
# language: pt

Funcionalidade: Realizar a codificação de dois cenários do aplicativo Whatsapp 
				para a avaliação de  habilidades em conceber cenários de testes.
	Contexto:
	Dado que estou com o aplicativo Whatsapp aberto

	Esquema do Cenário: Enviar mensagem utilizando o aplicativo Whatsapp
	E clico no botão de envio de mensagem
	E seleciono a primeira pessoa da lista de contatos
	Quando digito a "<MENSAGEM>"
	E clico em Enviar
	Então visualizo a mensagem enviada na tela de conversa
	Exemplos:
	|MENSAGEM|
	|Olá mundo!|
	|Hello World|

	Esquema do Cenário: Utilizador realiza a criação de um grupo utilizando o recurso do aplicativo
	E clico no botão que contem três pontos verticais
	E clico em Novo grupo
	Quando seleciono os contatos <"CONTATO1">, <"CONTATO2"> e <"CONTATO3">
	E defino o nome do grupo 
	Então visualizo a tela do grupo criado.
		Exemplos:
	|CONTATO1|CONTATO2|CONTATO3|
	|JOÃO    |MARIA	  | JOSÉ   |


